function createCard(name, location, description, pictureUrl, starts, ends) {
    return `
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${new Date(starts).toLocaleDateString()} -
        ${new Date(ends).toLocaleDateString()}
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        //  throw new Error("Failed to fetch conferences");
      } else {
        const data = await response.json();
        const columns = document.querySelectorAll('.col');

        for (let i = 0; i < data.conferences.length; i++) {
          const conference = data.conferences[i];
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);

          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const starts = details.conference.starts;
            const ends = details.conference.ends;
            const html = createCard(title, location, description, pictureUrl, starts, ends);
            const column = columns[i % columns.length];
            column.innerHTML += html;
          }
        }
      }
    } catch (error) {
    //   const errorMessage = `
    //     <div class="alert alert-danger" role="alert">
    //     An error occurred: ${error.message}
    //   </div>
    // `;
    // errorContainer.innerHTML = errorMessage;
      // Handle the error case
    }
  });
